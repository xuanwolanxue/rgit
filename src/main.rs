include!("manifest.rs");
use  std::env;
fn main() {
    println!("Hello, world!");
    let args: Vec<String> = env::args().collect();
    if args.len() == 1{
        return ;
    }

    let manifest = manifest_parser(&args[1]);

/*
    for (name, remote) in manifest.remote {
        print!("remote name: {:?}, remote.push: {:?}\n", name, remote.push.unwrap_or("not found".to_string()))
    }

    print!("defalut.sync_c: {:?}\n", manifest.default.sync_c);
*/
    print!("{:?}\n", manifest);
}

use serde::{Serialize, Deserialize};
use std::fs::File;
//use std::collections::HashMap;
use serde_with::serde_as;

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct Remote {
    fetch: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    push: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    review: Option<String>
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct DefaultInfo {
    sync_c: bool,
    sync_s: bool,
    remote: String,
    revision: String
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct PrjCommand {
    cmd: String,
    src: String,
    dst: String
}

#[serde_with::serde_as]
#[derive(Debug, PartialEq, Deserialize, Serialize)]
struct Commands(#[serde_as(as = "serde_with::VecSkipError<_>")] Vec<PrjCommand>);

#[serde_as]
#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct Project {
    path: String,
    remote: String,
    revision: String,
    sync_c: bool,
    sync_s: bool,
    //#[serde(skip_serializing_if = "Vec::is_empty")]
    #[serde(skip_serializing_if = "Option::is_none")]
    //#[serde_as(as = "serde_with::VecSkipError<_>")] 
    commands: Option<Commands>
}


#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct Manifest {
    remote: Vec<Remote>,
    default: DefaultInfo,
    project: Vec<Project>
}


fn manifest_parser(file_name: &str) -> Manifest {
    let yaml_file = File::open(file_name).unwrap();
    let manifest: Manifest = serde_yaml::from_reader(yaml_file).unwrap();
    return manifest;
}